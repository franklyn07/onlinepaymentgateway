Implemented an online payment gateway that depends on third party applications.

Then tested it using:
1. Unit Testing
2. Model Based Testing
3. Automated Web Testing (Using Selenium Driver and Cucumber)