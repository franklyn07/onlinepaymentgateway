package cps3230.payment_processor_system.model_based_testing;


import cps3230.payment_processing_system.back_end.CCInfo;
import cps3230.payment_processing_system.back_end.PaymentProcessor;
import nz.ac.waikato.modeljunit.*;
import nz.ac.waikato.modeljunit.coverage.ActionCoverage;
import nz.ac.waikato.modeljunit.coverage.StateCoverage;
import nz.ac.waikato.modeljunit.coverage.TransitionPairCoverage;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

import static org.junit.Assert.assertEquals;

public class modelTest implements FsmModel{

    //------------------------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------
    //Instance of our System Under Test
    private PaymentProcessor SUT = new PaymentProcessor();
    //Possible states of the model
    boolean unverified, verified_offline, authorised, voided, captured, refunded = false;


    //------------------------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------
    // This is used, so we test on the same transaction all the time
    Long transactionId = 12345L;

    public CCInfo getValidCCInfoInstance(){
        //Creating a valid date to use in the CCInfo
        Calendar today = Calendar.getInstance(); //gets today's date
        today.add(Calendar.DATE, 1); //adds one day
        SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");
        String futureDate = format.format(today.getTime());

        //An instance of CCInfo class to pass as a parameter to the SUT's verifyOffline()
        CCInfo validCCInfoInstance = new CCInfo(
                "John",
                "example-address",
                "Visa",
                "4555555555555",
                futureDate,
                "123");

        return validCCInfoInstance;
    }

    public CCInfo getInvalidCCInfoInstance(){
        //Creating an invalid date to use in the CCInfo
        String pastDate = "01-01-2001"; //in the past not future

        //An instance of CCInfo class to pass as a parameter to the SUT's verifyOffline()
        CCInfo invalidCCInfoInstance = new CCInfo(
                "John",
                "example-address",
                "Visa",
                "4555555555555",
                pastDate,
                "123"
        );

        return invalidCCInfoInstance;
    }

    //------------------------------------------------------------------------------------------------------------------
    //Implementing the FsmModel abstract methods (since it's an interface, we need to provide an implementation)
    //------------------------------------------------------------------------------------------------------------------
    public String getState(){
        if(unverified)
            return "UNVERIFIED";
        else if (verified_offline)
            return "VERIFIED_OFFLINE";
        else if(authorised)
            return "AUTHORISED";
        else if(voided)
            return "VOIDED";
        else if(captured)
            return "CAPTURED";
        else if(refunded)
            return "REFUNDED";
        else
            return "error in model";
    }

    /*
        This resets the model. If true is passed as a parameter, then the SUT is also reset.
        This is called randomly by the system itself.
     */
    public void reset(final boolean var1) {
        if(var1)
        {
            //resetting the SUT
            SUT = new PaymentProcessor();
        }

        //resetting all the model's booleans
        unverified = true;
        verified_offline = false;
        authorised = false;
        voided = false;
        captured = false;
        refunded = false;
    }


    //------------------------------------------------------------------------------------------------------------------
    //Transactions
    //------------------------------------------------------------------------------------------------------------------

    //** verifyOffline Successful
    public boolean verify_offlineGuard() {return getState().equals("UNVERIFIED");}
    public @Action void verify_offline() {

        CCInfo validCCInfoInstance = getValidCCInfoInstance();

        //Updating SUT
        SUT.verifyOffline(validCCInfoInstance);

        unverified = false; //updating the model
        verified_offline = true; //updating the model
        authorised = false;
        voided = false;
        captured = false;
        refunded = false;

        //SUT should be in VERIFIED_OFFLINE and not in UNVERIFIED
        assertEquals("verify_offline(); SUT is not in VERIFIED_OFFLINE", verified_offline, SUT.isInVerifiedOffline());
        assertEquals("verify_offline(); SUT is still in UNVERIFIED", unverified, SUT.isInUnverified());
    }

    /*
        This transition simulates/models a verifyOffline() call with a CCInfo with some invalid data
        The invalid data in the CCInfo in this example will be that the date is in the past
     */
    public boolean verify_offline_errorGuard() {return getState().equals("UNVERIFIED");}
    public @Action void verify_offline_error() {

        CCInfo invalidCCInfoInstance = getInvalidCCInfoInstance();

        //Updating SUT
        SUT.verifyOffline(invalidCCInfoInstance);

        unverified = true; //updating the model (the same)
        verified_offline = false;
        authorised = false;
        voided = false;
        captured = false;
        refunded = false;

        //SUT should be in UNVERIFIED and not in VERIFIED_OFFLINE
        assertEquals("verify_offline_error(); SUT is not in UNVERIFIED", unverified, SUT.isInUnverified());
        assertEquals("verify_offline_error(); SUT is in VERIFIED_OFFLINE", verified_offline, SUT.isInVerifiedOffline());
    }

    /*
        Authorisation successful

        This transition simulates/models a call to the decisionAuth() method with the first parameter to it being a
        positive long representing a transactionID, which in the actual SUT is supposed to be returned by the
        bankProxy.auth() method.
        But here since we want to model a succeeding authorisation call, we are hardcoding a positive long in the first
        parameter.
        The same applies for the third parameter, we are hardcoding a Long value, which simulates the transaction amount.

    */
    public boolean authoriseGuard() {return getState().equals("VERIFIED_OFFLINE");}
    public @Action void authorise() {

        CCInfo validCCInfoInstance = getValidCCInfoInstance();

        //Updating SUT
        SUT.decisionAuthReturn(transactionId, validCCInfoInstance, 12345L);

        unverified = false;
        verified_offline = false; //updating the model
        authorised = true; //updating the model
        voided = false;
        captured = false;
        refunded = false;

        //SUT should be in AUTHORISED and not in VERIFIED_OFFLINE
        assertEquals("authorise(); SUT is not in AUTHORISED", authorised, SUT.isInAuthorised());
        assertEquals("authorise(); SUT is still in VERIFIED_OFFLINE", verified_offline, SUT.isInVerifiedOffline());
    }

      /*
        This models an erranous authorisation of a transaction.
        All errors that auth() can raise, they will all leave the system in the same state; VERIFIED_OFFLINE. So we
        model all the errors with one transition in the state machine.

        We are using the -1 error code to test this (-1 : credit card details are invalid in any way)

        It simulates call to the decisionAuth() method with the first parameter to it being a negative
        (simulating an error coming from the bankProxy.auth())

        The third parameter (amount) is being hardcoded, because it doesn't make a difference here.

     */
    public boolean authorise_errorGuard() {return getState().equals("VERIFIED_OFFLINE");}
    public @Action void authorise_error() {

        //A valid CCInfo instance
        CCInfo validCCInfoInstance = getValidCCInfoInstance();

        //Updating SUT
        //The CCInfo was verified offline, but then rejected by the bank authorisation.
        SUT.decisionAuthReturn(-1L, validCCInfoInstance, 12345L);

        unverified = false;
        verified_offline = true; //updating the model (the same)
        authorised = false;
        voided = false;
        captured = false;
        refunded = false;

        //SUT should be in VERIFIED_OFFLINE and not in AUTHORISED
        assertEquals("authorise_error(): SUT is not in VERIFIED", verified_offline, SUT.isInVerifiedOffline());
        assertEquals("authorise_error(); SUT is in AUTHORISED", authorised, SUT.isInAuthorised());
    }

    /*
        Successful capture of the funds.

        This transition simulates/models a call to updateTransactionState_AfterCapture() with 0 passed as the
        first parameter, meaning that capture was successful.
     */
    public boolean captureGuard() {return getState().equals("AUTHORISED");}
    public @Action void capture() {

        //A valid CCInfo instance
        CCInfo validCCInfoInstance = getValidCCInfoInstance();

        //Updating SUT (we should update the SUT with all the method calls that would have happened in reality)
        SUT.decisionAuthReturn(transactionId, validCCInfoInstance, 12345L);
        SUT.updateTransactionState_AfterCapture(0, transactionId);

        unverified = false;
        verified_offline = false;
        authorised = false; //updating the model
        voided = false;
        captured = true; //updating the model
        refunded = false;

        //SUT should be in CAPTURED and not in AUTHORISED
        assertEquals("capture(); SUT is not in CAPTURED", captured, SUT.isInCaptured());
        assertEquals("capture(); SUT is still in AUTHORISED", authorised, SUT.isInAuthorised());
    }

    /*
        A capture() call which fails with error number -3 (the 7 day period has expired, thus voiding the transaction)
    */

    public boolean capture_period_expiredGuard(){return getState().equals("AUTHORISED");}
    public @Action void capture_period_expired() {

        //A valid CCInfo instance
        CCInfo validCCInfoInstance = getValidCCInfoInstance();

        // Updating SUT (note that for the transaction to actually be found in the transaction database, we need to
        // call the whole sequence of a typical PaymentProcessor, else the updateTransactionState_AfterCapture() won't
        // find the transaction in the database, and will fail).
        SUT.decisionAuthReturn(transactionId, validCCInfoInstance, 12345L);
        SUT.updateTransactionState_AfterCapture(-3, transactionId);

        unverified = false;
        verified_offline = false;
        authorised = false; //updating the model
        voided = true; //updating the model
        captured = false;
        refunded = false;

        //SUT should be in VOID and not in AUTHORISED
        assertEquals("capture_period_expired(); SUT is not in VOID", voided, SUT.isInVoid());
        assertEquals("capture_period_expired(); SUT is still in AUTHORISED", authorised, SUT.isInAuthorised());
    }

    /*
        A capture() call which fails with error code -4 (unknown error)
     */

    public boolean unknown_capture_errorGuard(){return getState().equals("AUTHORISED");}
    public @Action void unknown_capture_error() {

        //A valid CCInfo instance
        CCInfo validCCInfoInstance = getValidCCInfoInstance();

        SUT.decisionAuthReturn(transactionId, validCCInfoInstance, 12345L); //valid authorisation
        SUT.updateTransactionState_AfterCapture(-4, transactionId);

        unverified = false;
        verified_offline = false;
        authorised = true; //updating the model (the same)
        voided = false;
        captured = false;
        refunded = false;

        //SUT should be in AUTHORISED and not in CAPTURED
        assertEquals("unknown_capture_error(); SUT is not in AUTHORISED", authorised, SUT.isInAuthorised());
        assertEquals("unknown_capture_error(); SUT is in CAPTURED", captured, SUT.isInCaptured());
    }

    /*
        Capturing a transaction that has already been captured (error code -2)
     */
    public boolean capture_againGuard(){return getState().equals("CAPTURED");}
    public @Action void capture_again() {

        //A valid CCInfo instance
        CCInfo validCCInfoInstance = getValidCCInfoInstance();

        SUT.decisionAuthReturn(transactionId, validCCInfoInstance, 12345L); //valid authorisation
        SUT.updateTransactionState_AfterCapture(-2, transactionId);

        unverified = false;
        verified_offline = false;
        authorised = false;
        voided = false;
        captured = true; //updating the model (the same)
        refunded = false;

        //SUT should be in CAPTURED
        assertEquals("capture_again(); SUT is not in CAPTURED", captured, SUT.isInCaptured());
    }

    /*
        Refunding a transaction by calling updateTransactionState_AfterRefund() in the SUT and passing 0 (meaning
        that the bankProxy's refund() method was successful).
    */
    public boolean refundGuard() {return getState().equals("CAPTURED");}
    public @Action void refund() {

        //A valid CCInfo instance
        CCInfo validCCInfoInstance = getValidCCInfoInstance();

        //Updating SUT
        SUT.decisionAuthReturn(transactionId, validCCInfoInstance, 12345L); //authorise
        SUT.updateTransactionState_AfterCapture(0, transactionId); //capture
        SUT.updateTransactionState_AfterRefund(0, transactionId); //refund

        unverified = false;
        verified_offline = false;
        authorised = false;
        voided = false;
        captured = false; //Updating the model
        refunded = true; //Updating the model

        //SUT should be in REFUNDED and not in CAPTURE
        assertEquals("refund(); SUT is not in REFUNDED", refunded, SUT.isInRefunded());
        assertEquals("refund(); SUT is still in CAPTURED", captured, SUT.isInCaptured());
    }

    /*
        A refund() call which fails with error code -2 (transaction exists but has not been captured)
     */

    public boolean refund_without_captureGuard(){return getState().equals("AUTHORISED");}
    public @Action void refund_without_capture() {

        //A valid CCInfo instance
        CCInfo validCCInfoInstance = getValidCCInfoInstance();

        SUT.decisionAuthReturn(transactionId, validCCInfoInstance, 12345L); //authorisation
        SUT.updateTransactionState_AfterRefund(-2, transactionId); //invalid refund

        unverified = false;
        verified_offline = false;
        authorised = true; //updating the model (the same)
        voided = false;
        captured = false;
        refunded = false;

        //SUT should be in AUTHORISED and not in REFUNDED
        assertEquals("refund_without_capture(); SUT is not in AUTHORISED", authorised, SUT.isInAuthorised());
        assertEquals("refund_without_capture(); SUT is in REFUNDED", refunded, SUT.isInRefunded());
    }

    /*
        A refund() call which fails.
        For instance failing with -4 (requested refund amount is greater than the captured amount).
     */

    public boolean refund_errorGuard(){return getState().equals("CAPTURED");}
    public @Action void refund_error() {

        //A valid CCInfo instance
        CCInfo validCCInfoInstance = getValidCCInfoInstance();

        SUT.decisionAuthReturn(transactionId, validCCInfoInstance, 12345L); //authorisation
        SUT.updateTransactionState_AfterCapture(0, transactionId); //capturing
        SUT.updateTransactionState_AfterRefund(-4, transactionId); //invalid refund

        unverified = false;
        verified_offline = false;
        authorised = false;
        voided = false;
        captured = true; //updating the model (the same)
        refunded = false;

        //SUT should be in CAPTURED and not in REFUNDED
        assertEquals("refund_error(); SUT is not in CAPTURED", captured, SUT.isInCaptured());
        assertEquals("refund_error(); SUT is in REFUNDED", refunded, SUT.isInRefunded());
    }

    /*
        Calling refund() on a transaction which is already refunded.
     */

    public boolean refund_againGuard(){return getState().equals("REFUNDED");}
    public @Action void refund_again() {

        //A valid CCInfo instance
        CCInfo validCCInfoInstance = getValidCCInfoInstance();

        SUT.decisionAuthReturn(transactionId, validCCInfoInstance, 12345L); //authorisation
        SUT.updateTransactionState_AfterCapture(0, transactionId); //capturing
        SUT.updateTransactionState_AfterRefund(-3, transactionId); //invalid refund

        unverified = false;
        verified_offline = false;
        authorised = false;
        voided = false;
        captured = false;
        refunded = true; //updating the model (the same)

        //SUT should be in REFUNDED
        assertEquals("refund_again(); SUT is not in REFUNDED", refunded, SUT.isInRefunded());
    }



    //------------------------------------------------------------------------------------------------------------------
    //Test runner
    //------------------------------------------------------------------------------------------------------------------
    @Test
    public void PaymentProcessorModelRunner() {
        /* Creates a test generator that can generate random walks. A greedy random walk gives preference to transitions
           that have never been taken before. Once all transitions out of a state have been taken, it behaves the same
           as a random walk.
        */
        final Tester tester = new GreedyTester(new modelTest());

        /* This looks ahead N-levels ahead (3 is the default lookahead). It chooses the highest-value transition that is
        enabled from the current state. The value of each transition is determined by three parameters:
            -> NEWACTION: the value of a transition that has never been taken anywhere else in the graph
            -> NEWTRANS: if the action has been taken elsewhere
            -> The value of an action that has been taken before is the value of the state that it leads to,
               minus the number of times that the transition has been taken. So previously taken paths
               gradually become less and less attractive.
               The value of a state is 0 if it is the current state or if it is at least DEPTH steps away from the
               current state, otherwise it is the maximum value of its outgoing transitions.
               (note that in graph-theory the depth of a node is the number of edges from the node to the tree's
               root node)

         */
        //final Tester tester = new LookaheadTester(new modelTest());

        /*
            Tests a system by making random walks through the finite state machine model of the system.
         */
        //final RandomTester tester = new RandomTester(new modelTest());

        /*
            All round tester (check whether this is the Chinese Postman problem)
         */
        //final Tester tester = new AllRoundTester(new modelTest());
        //((AllRoundTester) tester).setLoopTolerance(4);

        //Allows for a random path each time the model is run.
        tester.setRandom(new Random());
        //Builds a model of our FSM to ensure that the coverage metrics are correct.
        tester.buildGraph();
        //This listener forces the test class to stop running as soon as a failure is encountered in the model.
        tester.addListener(new StopOnFailureListener());
        //This gives you printed statements of the transitions being performed along with the source and destination states.
        tester.addListener("verbose");
        /*
            Transition-pair; also known as 'Chow’s 1-switch coverage'
            This is the coverage of the valid two-transition pairs available. So for example a valid two-transition pair would be
            that from the UNVERIFIED, and then you transition using verify_offline() + authorise(), which will get you to the
            AUTHORISED state.
            https://rbcs-us.com/site/assets/files/1165/black-ast-state-diagrams-newsletter-2.pdf
         */
        tester.addCoverageMetric(new TransitionPairCoverage()); //Records the transition pair coverage i.e. the number of paired transitions traversed during the execution of the test.
        tester.addCoverageMetric(new StateCoverage()); //Records the state coverage i.e. the number of states which have been visited during the execution of the test.
        tester.addCoverageMetric(new ActionCoverage()); //Records the number of @Action methods which have ben executed during the execution of the test.

        //Generates 500 transitions
        tester.generate(500);
        //Prints the coverage metrics specified above.
        tester.printCoverage();
    }

}
