package cps3230.payment_processing_system.unit_testing;

import java.util.HashMap;
import java.util.Map;

import cps3230.payment_processing_system.back_end.CCInfo;
import cps3230.payment_processing_system.back_end.Transaction;
import cps3230.payment_processing_system.back_end.TransactionDatabase;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


public class TransactionDatabaseTests {

    TransactionDatabase transactionDatabase;

    @Before
    public void setup() {
        transactionDatabase = new TransactionDatabase();
    }

    @After
    public void teardown(){
        transactionDatabase = null;
    }

    @Test
    public void test_ConstructorWithParameter() {

        //this will be passed in the constructor of the TransactionDatabase object
        Map<Long, Transaction> alreadyExistingDatabase = new HashMap<Long, Transaction>();

        //Creating mock objects of transactions and stubbing the get_id method to be able to 'put' it in the alreadyExistingDatabase
        Transaction dummyTransaction1 = mock(Transaction.class);
        when(dummyTransaction1.get_id()).thenReturn(1L);
        Transaction dummyTransaction2 = mock(Transaction.class);
        when(dummyTransaction1.get_id()).thenReturn(2L);

        //adding the transactions to the database
        alreadyExistingDatabase.put(dummyTransaction1.get_id(), dummyTransaction1);
        alreadyExistingDatabase.put(dummyTransaction2.get_id(), dummyTransaction2);

        //creating the TransactionDatabase and passing the alreadyExisitngDatabase in const.
        transactionDatabase = new TransactionDatabase(alreadyExistingDatabase);

        assertEquals(alreadyExistingDatabase.size(), transactionDatabase.getNumberOfTransactions());
    }

    @Test
    public void test_GetNumberOfTransactions() {

        Map<Long, Transaction> mockMap = mock(Map.class);
        when(mockMap.size()).thenReturn(1);

        transactionDatabase = new TransactionDatabase(mockMap);

        assertEquals(1, transactionDatabase.getNumberOfTransactions());
    }

    @Test
    public void test_NewTransactionDbIsEmpty() {

        // No transactions were added to the database, so it should be empty
        assertEquals(0, transactionDatabase.getNumberOfTransactions());
    }

    @Rule
    public ExpectedException thrownException = ExpectedException.none();

    @Test
    public void test_SaveTransaction_nullTransaction() throws Exception {

        Transaction nullTransaction = null;

        thrownException.expect(Exception.class);
        thrownException.expectMessage("Transaction you are trying to save is null");

        try{
            transactionDatabase.saveTransaction(nullTransaction);
        }catch (Exception e)
        {
            throw e;
        }
    }

    @Test
    public void test_SaveTransaction_IncrementsCount() {
        int oldCount = transactionDatabase.getNumberOfTransactions();

        try{
            //add new transaction dummy object (the attribute values of the transaction are irrelevant)
            transactionDatabase.saveTransaction(mock(Transaction.class));

            assertEquals(oldCount+1, transactionDatabase.getNumberOfTransactions());

        }catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void test_SaveTransaction_SameTransactionTwice() {
        Transaction transaction = mock(Transaction.class);

        try{

            //add the same dummy object twice
            transactionDatabase.saveTransaction(transaction);
            transactionDatabase.saveTransaction(transaction);

            //saveTransaction() method should only have saved one pair
            assertEquals(1, transactionDatabase.getNumberOfTransactions());

        }catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void test_DeleteTransaction_DecrementsCount() {
        Transaction mockTransaction1 = mock(Transaction.class);
        Transaction mockTransaction2 = mock(Transaction.class);

        //giving the second mock transaction an ID, so we can use it to be able to delete it later on
        when(mockTransaction2.get_id()).thenReturn(123L);

        try{
            transactionDatabase.saveTransaction(mockTransaction1);
            transactionDatabase.saveTransaction(mockTransaction2);
            //count should be 2 up till this point

            transactionDatabase.deleteTransaction(mockTransaction2.get_id());

            assertEquals(1, transactionDatabase.getNumberOfTransactions());

        }catch(Exception e)
        {
            System.out.println(e.getMessage());
        }

    }

    @Test
    public void test_DeleteTransaction_TransactionNotFound() throws Exception{
        Transaction mockTransaction = mock(Transaction.class);
        when(mockTransaction.get_id()).thenReturn(123L);

        thrownException.expect(Exception.class);
        thrownException.expectMessage("Transaction not found when trying to delete");

        try{
            // Deleting the transaction twice will result in the second call raising an exception,
            // since it won't find the transaction.
            transactionDatabase.deleteTransaction(mockTransaction.get_id());
            transactionDatabase.deleteTransaction(mockTransaction.get_id());

        }catch (Exception e)
        {
            throw e;
        }
    }

    @Test
    public void test_UpdateTransactionStatus_successful() {
        //we do not care about its values
        CCInfo dummyCCInfo = mock(CCInfo.class);

        //This is a spy map, that we'll pass in with the constructor.
        //This will let us verify the size of the 'database' object for example.
        Map<Long, Transaction> databaseSpy = spy(new HashMap<Long, Transaction>());

        // the transaction instance had to be filled with real data, since updateTransactionStatus() method
        // will actually set the state to the passed value. So if we used a mock of the Transaction class,
        // the state attribute of it won't be accessible.
        Transaction transaction = new Transaction(123L, dummyCCInfo, 123L, "old-state");

        //Initialising the TransactionDatabase object again, so we pass in the spy object.
        transactionDatabase = new TransactionDatabase(databaseSpy);

        try{
            transactionDatabase.saveTransaction(transaction);

            transactionDatabase.updateTransactionStatus(transaction.get_id(), "new-state");

            String newState = databaseSpy.get(transaction.get_id()).get_state();

            assertEquals("new-state", newState);

        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void test_UpdateTransactionStatus_TransactionNotFound() throws Exception{

        thrownException.expect(Exception.class);
        thrownException.expectMessage("Transaction not found when updating status");

        try{

            //trying to update the state of a non-existing transaction with arbitrary ID 123
            //This should raise an exception
            transactionDatabase.updateTransactionStatus(123L, "new-state");

        }catch (Exception e)
        {
            throw e;
        }
    }

    @Test
    public void test_GetTransactionState() {

        CCInfo mockCCInfo = mock(CCInfo.class);

        Long transactionID = 123L;
        String transactionState = "example-state";

        Transaction transaction = new Transaction(transactionID, mockCCInfo, 123L, transactionState);

        try{
            transactionDatabase.saveTransaction(transaction);
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
        }

        String newState = transactionDatabase.getTransactionState(transactionID);

        assertEquals(transactionState, newState);
    }
}