package cps3230.payment_processing_system.unit_testing;

import cps3230.payment_processing_system.back_end.CCInfo;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class CCInfoTests {
    CCInfo ccInfo;
    String customerName, customerAddress, cardType, cardNumber, cardExpiryDate, cardCVV;

    @Before
    public void setup()
    {
        customerName = "John";
        customerAddress = "123, Street Name, City, Country";
        cardType = "American Express";
        cardNumber = "341234567891234";
        cardExpiryDate = "01/20";
        cardCVV = "007";

        ccInfo = new CCInfo(customerName, customerAddress, cardType, cardNumber, cardExpiryDate, cardCVV);
    }

    @After
    public void teardown()
    {
        ccInfo = null;
    }

    //Testing the constructor, and the individual getters per attribute
    @Test
    public void testCustomerNameGetter()
    {
        assertEquals(customerName, ccInfo.get_customerName());
    }

    @Test
    public void testCustomerAddressGetter()
    {
        assertEquals(customerAddress, ccInfo.get_customerAddress());
    }

    @Test
    public void testCardTypeGetter()
    {
        assertEquals(cardType, ccInfo.get_cardType());
    }

    @Test
    public void testCardNumberGetter()
    {
        assertEquals(cardNumber, ccInfo.get_cardNumber());
    }

    @Test
    public void testCardExpiryDateGetter()
    {
        assertEquals(cardExpiryDate, ccInfo.get_cardExpireyDate());
    }

    @Test
    public void testCardCVVGetter()
    {
        assertEquals(cardCVV, ccInfo.get_cardCVV());
    }
}
