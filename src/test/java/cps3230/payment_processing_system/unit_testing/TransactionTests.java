package cps3230.payment_processing_system.unit_testing;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import cps3230.payment_processing_system.back_end.CCInfo;
import cps3230.payment_processing_system.back_end.Transaction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TransactionTests {
    Transaction transaction;
    CCInfo dummy_ccinfo;
    long id;
    long amount;
    String state;


    @Before
    public void setup(){
        dummy_ccinfo = mock(CCInfo.class);
        id = 1L;
        amount = 4111111111111L;
        state = "Void";
        transaction = new Transaction(id, dummy_ccinfo, amount, state);
    }

    @After
    public void teardown(){
        transaction = null;
        dummy_ccinfo = null;
    }

    @Test
    public void test_get_id(){
        assertEquals(id,transaction.get_id());
    }

    @Test
    public void test_get_ccinfo(){
        assertEquals(dummy_ccinfo,transaction.get_info());
    }

    @Test
    public void test_get_amount(){
        assertEquals(amount,transaction.get_amount());
    }

    @Test
    public void test_get_state(){
        assertEquals(state,transaction.get_state());
    }

    @Test
    public void test_set_state(){
        String state = "Refunded";
        transaction.set_state("Refunded");
        assertEquals(state, transaction.get_state());
    }
}
