package cps3230.payment_processing_system.unit_testing;

import cps3230.payment_processing_system.back_end.BankProxy;
import cps3230.payment_processing_system.back_end.CCInfo;
import cps3230.payment_processing_system.back_end.PaymentProcessor;
import cps3230.payment_processing_system.back_end.TransactionDatabase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class PaymentProcessorTest {
    PaymentProcessor payment_processor;

    @Before
    public void setup(){
        payment_processor = new PaymentProcessor();
    }

    @After
    public void teardown(){
        payment_processor = null;
    }

    /*
    @Test
    public void test_Constructor_UnverifiedStateTrue() {
        assertTrue(payment_processor.Unverified);
    }

    @Test
    public void test_Constructor_VerifiedOfflineStateFalse() {
        assertEquals(false, payment_processor.Verified_offline);
    }
    */

    @Test
    public void test_luhn_algorithm_positive(){
        assertEquals(true,payment_processor.LuhnAlgorithm("1234567890003"));
    }

    @Test
    public void test_luhn_algorithm_negative(){
        assertEquals(false,payment_processor.LuhnAlgorithm("1234567890083"));
    }

    @Test
    public void test_verify_card_type_mastercard1_p(){
        assertEquals(true, payment_processor.verifyCardType("5155555555555555","Mastercard"));
    }

    @Test
    public void test_verify_card_type_mastercard2_p(){
        assertEquals(true, payment_processor.verifyCardType("5255555555555555","Mastercard"));
    }

    @Test
    public void test_verify_card_type_mastercard3_p(){
        assertEquals(true, payment_processor.verifyCardType("5355555555555555","Mastercard"));
    }

    @Test
    public void test_verify_card_type_mastercard4_p(){
        assertEquals(true, payment_processor.verifyCardType("5455555555555555","Mastercard"));
    }

    @Test
    public void test_verify_card_type_mastercard5_p(){
        assertEquals(true, payment_processor.verifyCardType("5565555555555555","Mastercard"));
    }

    @Test
    public void test_verify_card_type_mastercard_casing(){
        assertEquals(true, payment_processor.verifyCardType("5565555555555555","maStERcarD"));
    }

    @Test
    public void test_verify_card_type_masteracrd_invalid_length(){
        assertEquals(false, payment_processor.verifyCardType("55655555555","Mastercard"));
    }

    @Test
    public void test_verify_card_type_mastercard_invalid_number(){
        assertEquals(false, payment_processor.verifyCardType("37555555555555","Mastercard"));
    }


    @Test
    public void test_verify_card_type_express1_p(){
        assertEquals(true, payment_processor.verifyCardType("345555555555555","American Express"));
    }

    @Test
    public void test_verify_card_type_express2_p(){
        assertEquals(true, payment_processor.verifyCardType("375555555555555","American Express"));
    }

    @Test
    public void test_verify_card_type_express_casing(){
        assertEquals(true, payment_processor.verifyCardType("345555555555555","americAn eXpress"));
    }

    @Test
    public void test_verify_card_type_express_invalid_number(){
        assertEquals(false, payment_processor.verifyCardType("355555555555555","American Express"));
    }

    @Test
    public void test_verify_card_type_express_invalid_length(){
        assertEquals(false, payment_processor.verifyCardType("37555555555555","American Express"));
    }

    @Test
    public void test_verify_card_type_visa1_p(){
        assertEquals(true, payment_processor.verifyCardType("4555555555555","Visa"));
    }

    @Test
    public void test_verify_card_type_visa2_p() {
        assertEquals(true, payment_processor.verifyCardType("4555555555555555","Visa"));
    }

    @Test
    public void test_verify_card_type_visa_casing(){
        assertEquals(true, payment_processor.verifyCardType("4555555555555555","viSA"));
    }

    @Test
    public void test_verify_card_type_visa_invalid_number(){
        assertEquals(false, payment_processor.verifyCardType("355555555555555","Visa"));
    }

    @Test
    public void test_verify_card_type_visa_invalid_length(){
        assertEquals(false, payment_processor.verifyCardType("4555","Visa"));
    }

    @Test
    public void test_verify_card_type_invalid_CCType()
    {
        //"Vis" is an invalid card type
        assertEquals(false, payment_processor.verifyCardType("4555555555555555", "Vis"));
    }

    @Test
    public void test_checkExpiryDate_positive_1(){
        assertEquals(true, payment_processor.checkExpiryDate("12-13-2018","12-13-2018"));
    }

    @Test
    public void test_checkExpiryDate_positive_2(){
        assertEquals(true, payment_processor.checkExpiryDate("12-18-2018","12-13-2018"));
    }


    @Test
    public void test_checkExpiryDate_negative(){
        assertEquals(false, payment_processor.checkExpiryDate("12-2-2018","12-13-2018"));
    }

    @Test
    public void test_process_payment_bad_card_info()
    {
        //partial mock methodology
        CCInfo ccInfo = mock(CCInfo.class);
        PaymentProcessor dummy_processor = spy(payment_processor);
        BankProxy bankProxy = mock(BankProxy.class);

        doReturn(-1).when(dummy_processor).verifyOffline(ccInfo);

        // processPayment returns 1 if a user-generated error caused the problem (e.g. invalid expiry date)
        assertEquals(1, dummy_processor.processPayment(ccInfo, 5L, bankProxy));
    }

    @Test
    public void test_process_payment_auth_returns_neg_one()
    {
        //partial mock methodology
        BankProxy bankProxy = mock(BankProxy.class);
        CCInfo ccInfo = mock(CCInfo.class);
        PaymentProcessor dummy_processor = spy(payment_processor);

        doReturn(0).when(dummy_processor).verifyOffline(ccInfo);

        when(bankProxy.auth(eq(ccInfo),anyLong())).thenReturn(-1L);

        assertEquals(2, dummy_processor.processPayment(ccInfo, 5L, bankProxy));
    }

    @Test
    public void test_process_payment_auth_returns_neg_two()
    {
        //partial mock methodology
        BankProxy bankProxy = mock(BankProxy.class);
        CCInfo ccInfo = mock(CCInfo.class);
        PaymentProcessor dummy_processor = spy(payment_processor);

        doReturn(0).when(dummy_processor).verifyOffline(ccInfo);
        when(bankProxy.auth(eq(ccInfo),anyLong())).thenReturn(-2L);

        assertEquals(2, dummy_processor.processPayment(ccInfo, 5L, bankProxy));
    }

    @Test
    public void test_process_payment_auth_returns_neg_three()
    {
        //partial mock methodology
        BankProxy bankProxy = mock(BankProxy.class);
        CCInfo ccInfo = mock(CCInfo.class);
        PaymentProcessor dummy_processor = spy(payment_processor);

        doReturn(0).when(dummy_processor).verifyOffline(ccInfo);

        when(bankProxy.auth(eq(ccInfo),anyLong())).thenReturn(-3L);

        assertEquals(2, dummy_processor.processPayment(ccInfo, -5L, bankProxy));
    }

    @Test
    public void test_process_payment_auth_returns_transaction_id()
    {
        //partial mock methodology
        BankProxy bankProxy = mock(BankProxy.class);
        CCInfo ccInfo = mock(CCInfo.class);
        PaymentProcessor dummy_processor = spy(payment_processor);

        doReturn(0).when(dummy_processor).verifyOffline(ccInfo);

        when(bankProxy.auth(eq(ccInfo),anyLong())).thenReturn(5L);

        assertEquals(0, dummy_processor.processPayment(ccInfo, -5L, bankProxy));
    }

    @Test
    public void test_updateTransactionState_AfterCapture_positive_transaction_id() {
        //spy Transaction Database to check that it's transactions are being called
        TransactionDatabase transactionDatabaseSpy = spy(TransactionDatabase.class);
        payment_processor.transactionDatabase = transactionDatabaseSpy;

        long transaction_id = 5L;

        payment_processor.updateTransactionState_AfterCapture(0,transaction_id);

        try {
            verify(transactionDatabaseSpy, times(1)).updateTransactionStatus(transaction_id, "Captured");
        } catch (Exception e) {
        }
    }

    @Test
    public void test_updateTransactionState_AfterCapture_negative_one() {
        //spy Transaction Database to check that it's transactions are being called
        TransactionDatabase transactionDatabaseSpy = spy(TransactionDatabase.class);
        payment_processor.transactionDatabase = transactionDatabaseSpy;

        long transaction_id = 5L;

        payment_processor.updateTransactionState_AfterCapture(-1,transaction_id);

        try {
            verify(transactionDatabaseSpy, times(1)).deleteTransaction(transaction_id);
        } catch (Exception e) {
        }
    }

    @Test
    public void test_updateTransactionState_AfterCapture_negative_two() {
        //spy Transaction Database to check that it's transactions are being called
        TransactionDatabase transactionDatabaseSpy = spy(TransactionDatabase.class);
        payment_processor.transactionDatabase = transactionDatabaseSpy;

        long transaction_id = 5L;

        payment_processor.updateTransactionState_AfterCapture(-2,transaction_id);

        try {
            verify(transactionDatabaseSpy, times(1)).updateTransactionStatus(transaction_id,"Captured");
        } catch (Exception e) {
        }
    }

    @Test
    public void test_updateTransactionState_AfterCapture_negative_three() {
        //spy Transaction Database to check that it's transactions are being called
        TransactionDatabase transactionDatabaseSpy = spy(TransactionDatabase.class);
        payment_processor.transactionDatabase = transactionDatabaseSpy;

        long transaction_id = 5L;

        payment_processor.updateTransactionState_AfterCapture(-3,transaction_id);

        try {
            verify(transactionDatabaseSpy, times(1)).updateTransactionStatus(transaction_id,"Void");
        } catch (Exception e) {
        }
    }

    @Test
    public void test_updateTransactionState_AfterRefund_positive() {
        //spy Transaction Database to check that it's transactions are being called
        TransactionDatabase transactionDatabaseSpy = spy(TransactionDatabase.class);
        payment_processor.transactionDatabase = transactionDatabaseSpy;

        long transaction_id = 5L;

        payment_processor.updateTransactionState_AfterRefund(0,transaction_id);

        try {
            verify(transactionDatabaseSpy, times(1)).updateTransactionStatus(transaction_id,"Refunded");
        } catch (Exception e) {
        }
    }

    @Test
    public void test_updateTransactionState_AfterRefund_negative_one() {
        //spy Transaction Database to check that it's transactions are being called
        TransactionDatabase transactionDatabaseSpy = spy(TransactionDatabase.class);
        payment_processor.transactionDatabase = transactionDatabaseSpy;

        long transaction_id = 5L;

        payment_processor.updateTransactionState_AfterRefund(-1,transaction_id);

        try {
            verify(transactionDatabaseSpy, times(1)).deleteTransaction(transaction_id);
        } catch (Exception e) {
        }
    }

    @Test
    public void test_updateTransactionState_AfterRefund_negative_two() {
        //spy Transaction Database to check that it's transactions are being called
        TransactionDatabase transactionDatabaseSpy = spy(TransactionDatabase.class);
        payment_processor.transactionDatabase = transactionDatabaseSpy;

        long transaction_id = 5L;

        payment_processor.updateTransactionState_AfterRefund(-2,transaction_id);

        try {
            verify(transactionDatabaseSpy, times(1)).updateTransactionStatus(transaction_id,"Authorised");
        } catch (Exception e) {
        }
    }

    @Test
    public void test_updateTransactionState_AfterRefund_negative_three() {
        //spy Transaction Database to check that it's transactions are being called
        TransactionDatabase transactionDatabaseSpy = spy(TransactionDatabase.class);
        payment_processor.transactionDatabase = transactionDatabaseSpy;

        long transaction_id = 5L;

        payment_processor.updateTransactionState_AfterRefund(-3,transaction_id);

        try {
            verify(transactionDatabaseSpy, times(1)).updateTransactionStatus(transaction_id,"Refunded");
        } catch (Exception e) {
        }
    }

    @Test
    public void test_updateTransactionState_AfterRefund_negative_four() {
        //spy Transaction Database to check that it's transactions are being called
        TransactionDatabase transactionDatabaseSpy = spy(TransactionDatabase.class);
        payment_processor.transactionDatabase = transactionDatabaseSpy;

        long transaction_id = 5L;

        payment_processor.updateTransactionState_AfterRefund(-4,transaction_id);

        try {
            verify(transactionDatabaseSpy, times(1)).updateTransactionStatus(transaction_id,"Captured");
        } catch (Exception e) {
        }
    }

    @Test
    public void test_check_fields_not_empty_positive() throws Exception{
        CCInfo ccInfo = new CCInfo(
                "John",
                "example-address",
                "American Express",
                "345555555555555",
                "12-20-2018",
                "001");
        assertEquals(true, payment_processor.checkFields((ccInfo)));
    }

    @Test
    public void test_check_fields_not_empty_negative() throws Exception{
        CCInfo ccInfo = new CCInfo(
                "John",
                "example-address",
                "",
                "345555555555555",
                "12-10-2018",
                "");
        assertEquals(false, payment_processor.checkFields((ccInfo)));
    }

    @Test
    public void test_verify_offline_positive(){
        Calendar today = Calendar.getInstance(); //gets today's date
        today.add(Calendar.DATE, 1); //adds one day

        SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");
        String futureDate = format.format(today.getTime());

        CCInfo ccInfo = new CCInfo(
                "John",
                "example-address",
                "Visa",
                "4555555555555",
                futureDate,
                "123");
        assertEquals(0,payment_processor.verifyOffline(ccInfo));
    }

    @Test
    public void test_verify_offline_negative_check_fields(){
        Calendar today = Calendar.getInstance(); //gets today's date
        today.add(Calendar.DATE, 1); //adds one day

        SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");
        String futureDate = format.format(today.getTime());

        CCInfo ccInfo = new CCInfo(
                "Edward",
                "example-address",
                "",
                "4555555555555",
                futureDate,
                "123");
        assertEquals(-4,payment_processor.verifyOffline(ccInfo));
    }

    @Test
    public void test_verify_offline_negative_expiry_date(){
        CCInfo ccInfo = new CCInfo(
                "John",
                "example-address",
                "Visa",
                "4555555555555",
                "20-12-2017",
                "123");
        assertEquals(-3,payment_processor.verifyOffline(ccInfo));
    }

    @Test
    public void test_verify_offline_negative_card_details(){
        Calendar today = Calendar.getInstance(); //gets today's date
        today.add(Calendar.DATE, 1); //adds one day

        SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");
        String futureDate = format.format(today.getTime());

        CCInfo ccInfo = new CCInfo(
                "John",
                "example-address",
                "Mastercard",
                "4555555555555",
                futureDate,
                "123");
        assertEquals(-2,payment_processor.verifyOffline(ccInfo));
    }

    @Test
    public void test_verify_offline_negative_Luhn(){
        Calendar today = Calendar.getInstance(); //gets today's date
        today.add(Calendar.DATE, 1); //adds one day

        SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");
        String futureDate = format.format(today.getTime());

        CCInfo ccInfo = new CCInfo(
                "John",
                "example-address",
                "Visa",
                "4555553555555",
                futureDate,
                "123");
        assertEquals(-1,payment_processor.verifyOffline(ccInfo));
    }

    @Test
    public void test_log_missingField()
    {
        PaymentProcessor paymentProcessor = mock(PaymentProcessor.class);
        CCInfo dummy_ccinfo = mock(CCInfo.class);

        when(paymentProcessor.verifyOffline(dummy_ccinfo)).thenReturn(-4);

        assertEquals(true, payment_processor.log(paymentProcessor.verifyOffline(dummy_ccinfo)));
    }

    @Test
    public void test_log_expiredCard()
    {
        PaymentProcessor paymentProcessor = mock(PaymentProcessor.class);
        CCInfo dummy_ccinfo = mock(CCInfo.class);

        when(paymentProcessor.verifyOffline(dummy_ccinfo)).thenReturn(-3);

        assertEquals(true, payment_processor.log(paymentProcessor.verifyOffline(dummy_ccinfo)));
    }

    @Test
    public void test_log_prefixAndCCType_NoMatch()
    {
        PaymentProcessor paymentProcessor = mock(PaymentProcessor.class);
        CCInfo dummy_ccinfo = mock(CCInfo.class);

        when(paymentProcessor.verifyOffline(dummy_ccinfo)).thenReturn(-2);

        assertEquals(true, payment_processor.log(paymentProcessor.verifyOffline(dummy_ccinfo)));
    }

    @Test
    public void test_log_LuhnsNotSatisfied()
    {
        PaymentProcessor paymentProcessor = mock(PaymentProcessor.class);
        CCInfo dummy_ccinfo = mock(CCInfo.class);

        when(paymentProcessor.verifyOffline(dummy_ccinfo)).thenReturn(-1);

        assertEquals(true, payment_processor.log(paymentProcessor.verifyOffline(dummy_ccinfo)));
    }

    @Test
    public void test_log_EverythingValid()
    {
        PaymentProcessor paymentProcessor = mock(PaymentProcessor.class);
        CCInfo dummy_ccinfo = mock(CCInfo.class);

        when(paymentProcessor.verifyOffline(dummy_ccinfo)).thenReturn(0);

        assertEquals(false, payment_processor.log(paymentProcessor.verifyOffline(dummy_ccinfo)));
    }
}
