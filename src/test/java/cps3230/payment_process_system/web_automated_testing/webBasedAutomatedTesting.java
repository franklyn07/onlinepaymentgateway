package cps3230.payment_process_system.web_automated_testing;

import static org.junit.Assert.*;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class webBasedAutomatedTesting {
    //Object holding the driver instance
    WebDriver driver;
    //Object holding the element we are searching for
    WebElement element;

    //thread that halts the test from proceeding with its execution for a set amount of seconds
    public void sleep(int seconds){
        try{
            Thread.sleep(seconds*1000);
        }catch (Exception e){}
    }

    //function that updates the given field given field name and data
    public void updateField(String fieldname, String data){
        //get element tagged by name
        element = driver.findElement(By.name(fieldname));
        //clear element contents
        element.clear();
        //fil element with a correct name
        element.sendKeys(data);
    }

    //return data in a field
    public String getText(String fieldname){
        element = driver.findElement(By.name(fieldname));
        return element.getText();
    }

    //function that selects a specific card type from the drop down menu
    public void selectCard(String cardType){
        Select select = new Select(driver.findElement(By.tagName("select")));
        select.selectByVisibleText(cardType);
    }

    //get response from form
    public String getResponse(){
        //submit the form with the injected contents
        element.submit();
        sleep(2);
        //get the h1 tag from the new dom contents - which were updated by submitting the form)
        element = driver.findElement(By.tagName("h1"));
        //get the text value of that tag
        String response = element.getText();

        return response;
    }

    //get tomorrow's date to make sure expiry date is not invalid when not required as such
    public String getTomorrowDate(){
        Calendar today = Calendar.getInstance(); //gets today's date
        today.add(Calendar.DATE, 1); //adds one day

        SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");
        String futureDate = format.format(today.getTime());

        return futureDate;
    }

    @Before
    public void setup(){
        //to be able to run this test download the selenium chrome driver
        //extract it and store it on the desktop in a file named driver_selenium
        System.setProperty("webdriver.chrome.driver","C:/Users/Owner/Desktop/driver-selenium/chromedriver.exe");
        //create an instance of the chrome driver
        driver = new ChromeDriver();
    }

    @After
    public void tearDown(){
        //quit the driver
        driver.quit();
    }

    @Given("^I am a user trying to process a payment$")
    public void iAmAUserTryingToProcessAPayment() throws Throwable{
        //get the website hosting the form, using the driver
        driver.get("http://localhost:7777/");
        //allow for page to load
        sleep(2);

        updateField("name", "Tom");
        updateField("address", "278, Triq Hal Saflieni, Dingli");
        selectCard("Mastercard");
        updateField( "card_no", "5125466666660008");
        updateField("expiry_date", getTomorrowDate());
        updateField("cvv_code", "123");
        updateField("amount", "15006");
    }

    //correct data feature
    @When("^I submit correct details$")
    public void iSubmitCorrectDetails(){
        // in given correct details are already supplied
    }

    @Then("^I should be told that the payment was successful$")
    public void iShouldBeToldPaymenSuccesful(){
        String response = getResponse();

        //compare obtained response with actual page response
        assertEquals(response, "Transaction is Successful");
    }

    //missing fields capture feature
    @When("^I submit a form with all data except ([^\"]*)$")
    public void iSubmitAllDataExcept(String fieldname){
        updateField(fieldname, "");
    }

    @Then("^I should be told that all fields are required$")
    public void iShouldBeToldMissingFields(){
        String response = getResponse();

        //compare obtained response with actual page response
        assertEquals(response, "Missing Fields! Please check all fields are filled! " +
                "Try resubmission resubmission by Pressing Back.");
    }

    //Invalid data Feature
    @When("^I submit a form with invalid ([^\"]*) which the processing system rejects$")
    public void iSubmitInvalidData(String fieldname){
        //we use default correct data and change it's card type, such that the card number doesn t match the card type
        if (fieldname.equals("card_type")){
            selectCard("American Express");
        }else if (fieldname.equals("card_no")) {
            //keep same card type (as in default) and input a wrong card number
            updateField("card_no", "5125466666660007");
        }else{
            //keep correct input data except for expiry date
            updateField("expiry_date", "12-18-2016");
        }
    }

    @Then("^I should be told that there was an error of type ([^\"]*)$")
    public void iShouldBeToldErrorType(String errorType){
        String response = getResponse();

        assertEquals(response, "Invalid " + errorType + "! Try resubmission by Pressing Back.");
    }


    //Valid different types of cards feature
    @When("^I submit correct details using a ([^\"]*) card$")
    public void iSubmitCorrectCardData(String cardType){
        if(cardType.equals("Mastercard")){
            updateField("card_no","5325466666660006");
        }else if(cardType.equals("Visa")){
            updateField("card_no","4125466666660001");
        }else{
            updateField("card_no","341111111111004");
        }
        selectCard(cardType);
    }

    // clear button feature
    @When("^I fill in the form$")
    public void iFillInForm(){
        updateField("name", "Tom");
        updateField("address", "278, Triq Hal Saflieni, Dingli");
        selectCard("Visa");
        updateField( "card_no", "null");
        updateField("expiry_date", getTomorrowDate());
        updateField("cvv_code", "wrong");
        updateField("amount", "15006");
    }

    @And("^click on the clear button$")
    public void iClickOnClearButton(){
        driver.findElement(By.tagName("button")).click();
    }

    @Then("^form data should be cleared$")
    public void formShouldBeCleared(){
        assertEquals("",getText("name"));
        assertEquals("",getText("address"));
        assertEquals("",getText("card_no"));
        assertEquals("",getText("expiry_date"));
        assertEquals("", getText("cvv_code"));
        assertEquals("",getText("amount"));
    }
}
