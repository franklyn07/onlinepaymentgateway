Feature: Payment process

	I want to be able to process a payment

	Scenario: Correct Card Details
		Given I am a user trying to process a payment
		When I submit correct details
		Then I should be told that the payment was successful

	Scenario Outline: Processing missing fields
		Given I am a user trying to process a payment
		When I submit a form with all data except <fieldname>
		Then I should be told that all fields are required

		Examples:
		    |   fieldname  |
		    |   name       |
		    |   address    |
		    |   card_no    |
		    |   expiry_date|
		    |   cvv_code   |

	Scenario Outline: Processing invalid card data
		Given I am a user trying to process a payment
		When I submit a form with invalid <fieldname> which the processing system rejects
		Then I should be told that there was an error of type <type>

        Examples:
    		| fieldname | type |
    		| card_type | Card Type |
    		| card_no | Card Number |
    		| expiry_date | Expiry Date |

	Scenario Outline: Processing different card types
		Given I am a user trying to process a payment
		When I submit correct details using a <card-type> card
		Then I should be told that the payment was successful

        Examples:
    		| card-type |
	    	| American Express |
		    | Mastercard |
		    | Visa |

	Scenario: Clearing incorrect data
		Given I am a user trying to process a payment
		When I fill in the form
		And click on the clear button
		Then form data should be cleared