package cps3230.payment_processing_system.web_interface;

import cps3230.payment_processing_system.back_end.BankProxy;
import cps3230.payment_processing_system.back_end.CCInfo;
import cps3230.payment_processing_system.back_end.PaymentProcessor;
import io.javalin.Context;
import io.javalin.Handler;
import io.javalin.Javalin;
import org.jetbrains.annotations.NotNull;

public class Main {

    //Instance of paymentProcessor. Used as static since  we will be using it throughout.
    static PaymentProcessor paymentProcessor = new PaymentProcessor();

    //Proxy implementation, to imitate bank outcomes.
    static BankProxy bankProxy = new BankProxy() {
        public long auth(CCInfo info, long amount) {
            return 0;
        }

        public int capture(long transaction_id) {
            return 0;
        }

        public int refund(long transaction_id, long amount) {
            return 0;
        }
    };

    public static void main(String[] args){

        // create server on port 7777 with the form source being located in the specified path relative to target folder created by maven
        Javalin app = Javalin.create()
                .enableStaticFiles("cps3230/payment_processing_system/web_interface")
                .start(7777);

        //since actions are relative, when browser will create post request on index.html, it will trigger the following function
        //this is possible throught the path piping aka through /process-payment
        app.post("/process-payment", new Handler() {
            public void handle(@NotNull Context ctx) throws Exception {
                //read data from index.html form
                String name = ctx.formParam("name");
                String address = ctx.formParam("address");
                String type = ctx.formParam("card_type");
                String card_number = ctx.formParam("card_no");
                String expiry_date = ctx.formParam("expiry_date");
                String cvv_code = ctx.formParam("cvv_code");

                //create ccinfo instance using such data
                CCInfo ccInfo = new CCInfo(name, address, type, card_number, expiry_date, cvv_code);

                //try to process payment using the user inputted data
                int value = paymentProcessor.processPayment(ccInfo,Long.valueOf(ctx.formParam("amount")), bankProxy);

                //if processPayment() returns a success, user is instructed appropriately
                //if processPayment() returns a bank generated error, user is instructed appropriately
                //if processPayment() returns a user generated error, verifyOffline() is rerun such that,
                //we can instruct the user what the error he is commiting is.
                if (value == 0){
                    ctx.html("<h1>Transaction is Successful</h1>");
                }else if(value == 2){
                    ctx.html("<h1>Bank denied transaction. Try again later!</h1>");
                }else{
                    int point_of_failure = paymentProcessor.verifyOffline(ccInfo);
                    switch(point_of_failure){
                        case -4:
                            ctx.html("<h1>Missing Fields! Please check all fields are filled! Try resubmission resubmission by Pressing Back.</h1>");
                            break;
                        case -3:
                            ctx.html("<h1>Invalid Expiry Date! Try resubmission by Pressing Back.</h1>");
                            break;
                        case -2:
                            ctx.html("<h1>Invalid Card Type! Try resubmission by Pressing Back.</h1>");
                            break;
                        case -1:
                            ctx.html("<h1>Invalid Card Number! Try resubmission by Pressing Back.</h1>");
                            break;
                    }
                }
            }
        });


    }


}
