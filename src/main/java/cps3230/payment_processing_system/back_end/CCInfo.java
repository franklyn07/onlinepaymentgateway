package cps3230.payment_processing_system.back_end;

public class CCInfo {
    private String customerName;
    private String customerAddress;
    private String cardType;
    private String cardNumber;
    private String cardExpireyDate;
    private String cardCVV;

    //constructor - no need for setters since Transaction variables will not be altered
    public CCInfo(String customerName, String customerAddress, String cardType, String cardNumber, String cardExpireyDate, String cardCVV){
        this.customerName = customerName;
        this.customerAddress =customerAddress;
        this.cardType = cardType;
        this.cardNumber = cardNumber;
        this.cardExpireyDate = cardExpireyDate;
        this.cardCVV = cardCVV;
    }

    //getters for variables
    public String get_customerName(){
        return customerName;
    }
    public String get_customerAddress(){
        return customerAddress;
    }
    public String get_cardType(){
        return cardType;
    }
    public String get_cardNumber(){ return cardNumber; }
    public String get_cardExpireyDate(){
        return cardExpireyDate;
    }
    public String get_cardCVV(){
        return cardCVV;
    }
}
