package cps3230.payment_processing_system.back_end;


import java.io.FileWriter;
import java.util.Date;
import java.text.SimpleDateFormat;

public class PaymentProcessor {

    public TransactionDatabase transactionDatabase = new TransactionDatabase();

    Long transactionId;

    // Since in our code we never save transactions with states such as "start" and "Verified_Offline",
    // we create these two booleans, so that in the model we can assert to these booleans instead of asserting
    // to the actual states saved in the transactionDatabase.
    public boolean Unverified, Verified_offline = false;

    public PaymentProcessor(){

        this.Unverified = true;
    }

    public int processPayment(CCInfo info, long amount, BankProxy bankProxy) {

        long authReturn;

        int verifyOfflineReturn = verifyOffline(info);

        if(verifyOfflineReturn < 0)
        {
            //Verify offline will return -4, -3, -2, -1, or 0
            //Its return value will be passed to log() which adds logs to file accordingly
            log(verifyOfflineReturn);
            return 1;
        }

        authReturn = bankProxy.auth(info, amount);

        //decides whether to log, or save the transaction in the database
        boolean return_val = decisionAuthReturn(authReturn, info, amount);

        if(return_val){
            return 0;
        }else{
            return 2;
        }
    }

    //verify card details offline
    public int verifyOffline(CCInfo info){
        //get today's date
        Date now = new Date();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("M-d-yyyy");
        String date_today = dateFormatter.format(now);

        if(!checkFields(info)){

            Unverified = true;
            Verified_offline = false;

            return -4;
        }else if (!checkExpiryDate(info.get_cardExpireyDate(),date_today)){

            Unverified = true;
            Verified_offline = false;

            return -3;
        }else if (!verifyCardType(info.get_cardNumber(),info.get_cardType())){

            Unverified = true;
            Verified_offline = false;

            return -2;
        }else if (!LuhnAlgorithm(info.get_cardNumber())){

            Unverified = true;
            Verified_offline = false;

            return -1;
        }else{

            // if verifyOffline has succeeded (all the checks have passed), then we need to change the state from Unverified to Verified_Offline
            Unverified = false;
            Verified_offline = true;

            return 0;
        }
    }

    //verify all fields have an input
    public boolean checkFields(CCInfo info){
        String field = info.get_customerName();
        if (field.length() <= 0){
            return false;
        }
        field = info.get_customerAddress();
        if (field.length() <= 0){
            return false;
        }
        field = info.get_cardType();
        if (field.length() <= 0){
            return false;
        }
        field = info.get_cardNumber();
        if (field.length() <= 0){
            return false;
        }
        field = info.get_cardExpireyDate();
        if (field.length() <= 0){
            return false;
        }
        field = info.get_cardCVV();
        if (field.length() <= 0){
            return false;
        }
        return true;
    }

    //verify date
    public boolean checkExpiryDate(String expiry_date, String date_today){
        SimpleDateFormat sdf = new SimpleDateFormat("M-d-yyyy");
        try {
            Date expiryDate = sdf.parse(expiry_date);
            Date dateToday = sdf.parse(date_today);


            if(expiryDate.compareTo(dateToday)<0){
                return false;
            }else{
                return true;
            }
        }catch(Exception e){
            System.out.println("Could not parse date. Date is incorrect format");
            return false;
        }
    }

    //verify that card type and prefix match
    public boolean verifyCardType(String account_num, String card_type){
        char [] account_char = account_num.toCharArray();

        if ("american express".equalsIgnoreCase(card_type)){
            if(account_char[0]!='3'){
                return false;
            }else{
                if(account_char[1]=='4' || account_char[1] == '7'){
                    if(account_num.length()==15){
                        return true;
                    }
                }
                return false;
            }
        }else if("visa".equalsIgnoreCase(card_type)){
            if(account_char[0]=='4'){
                if(account_num.length()==13 || account_num.length()==16){
                    return true;
                }
            }
            return false;
        }else if("mastercard".equalsIgnoreCase(card_type)){
            if(account_char[0]!='5'){
                return false;
            }else{
                if(Character.getNumericValue(account_char[1])>=1 && Character.getNumericValue(account_char[1])<=5){
                    if(account_num.length() == 16){
                        return true;
                    }
                }
                return false;
            }
        }else{
            return false;
        }
    }

    //verify card number
    public boolean LuhnAlgorithm(String account_num){
        //checking whether the length of account num is even or odd
        int remainder;
        int length = account_num.length();
        if (length%2==0){
            remainder = 0;
        }else{
            remainder = 1;
        }

        //convert account number in a list of chars - each char represents one int
        char [] account_char = account_num.toCharArray();
        int [] account_int = new int[account_char.length] ;

        //convert each char to integer
        for (int i = 0; i < account_char.length; i++){
            account_int[i] = account_char[i] - '0';
        }

        for (int j = account_int.length-1; j >=0; j--){
            //doubling appropriate values according to algorithm
            if( j%2 == remainder){
                int new_val = account_int[j] * 2;
                if(new_val > 9){
                    //converting value back to between 0 and 9
                    account_int[j] = new_val -9;
                }else{
                    account_int[j] = new_val;
                }
            }
        }

        //calculating checksum
        long check_sum = 0;
        for (int i = 0; i < account_int.length; i++){
            check_sum += account_int[i];
        }


        if (check_sum % 10 ==0){
            return true;
        }else{
            return false;
        }
    }

    public boolean decisionAuthReturn(long authReturn, CCInfo info, long amount){

        //if there was an error in the auth(), then the return is between -1 and -3
        //when there is an error, then we log it with a different error number, since the log already has error codes between -1 and -3
        if(-3L <= authReturn && authReturn <= -1L)
        {
            log((int)authReturn-4);

            Unverified = false;
            Verified_offline = true;

            return false;
        }else {
            //else if there was no errors in the auth, then we create a new instance of a Transaction and save it in the database
            try {

                //we save the transactionId so we can use it when we are retrieving the transaction state
                //from the transaction database.
                transactionId = authReturn;

                transactionDatabase.saveTransaction(new Transaction(authReturn, info, amount, "Authorised"));

                //From here onwards, we will not be using the boolean Unverified and Verified_Offline to check the state of the transaction.
                //We will be using the 'state' attribute of the Transaction saved in the transaction database.
                //Thus, we set both Unverified and Verified_Offline as false.
                Unverified = false;
                Verified_offline = false;

            }catch(Exception e){
                log(1);
            }
            return true;
        }
    }

    public boolean log(int errorNum){

        /*
        Error codes and what they represent:

            OFFLINE VERIFICATION ERRORS
                -1 : Luhn's Algorithm not satisfied
                -2 : CC Prefix and CC Type do not match
                -3 : CC Expired
                -4 : Missing data fields in the CC Info

            AUTHORIZATION ERRORS
                -5 : CC Details are invalid in any way
                -6 : CC Details okay, but not enough funds in bank account
                -7 : Unknown error

            CAPTURE ERRORS
                -8 : Transaction does not exist
                -9 : Transaction exists but has already been captured
                -10: Transaction exists but 7-day period has expired (transaction will be voided)
                -11: Unkown error

            REFUND ERRORS
                -12 : Transaction does not exist
                -13 : Transaction exists but has not been captured
                -14 : Transaction exists but a refund was already processed
                -15 : Refund amount is greater than the amount captured
                -16 : Unknown error
         */

        FileWriter fw;

        try{
            if(errorNum != 0) {
                //opens the file in append mode
                fw = new FileWriter("ProcessPayment.log", true);


                switch (errorNum) {

                    //OFFLINE VERIFICATION ERRORS
                    case -1: {
                        fw.write("OFFLINE VERIFICATION ERROR: Luhn's algorithm not satisfied\n");
                        break;
                    }
                    case -2: {
                        fw.write("OFFLINE VERIFICATION ERROR: Credit card number prefix and the credit card type do not match\n");
                        break;
                    }
                    case -3: {
                        fw.write("OFFLINE VERIFICATION ERROR: Credit card is expired\n");
                        break;
                    }
                    case -4: {
                        fw.write("OFFLINE VERIFICATION ERROR: Not all credit card information has been entered\n");
                        break;
                    }

                    //AUTHORIZATION ERRORS
                    case -5: {
                        fw.write("AUTHORIZATION ERROR: Credit card details are invalid\n");
                        break;
                    }
                    case -6: {
                        fw.write("AUTHORIZATION ERROR: Not enough funds in bank account\n");
                        break;
                    }
                    case -7: {
                        fw.write("AUTHORIZATION ERROR: Unknown error\n");
                        break;
                    }

                    //CAPTURE ERRORS
                    case -8: {
                        fw.write("CAPTURE ERROR: Transaction does not exist\n");
                        break;
                    }
                    case -9: {
                        fw.write("CAPTURE ERROR: Transaction already captured\n");
                        break;
                    }
                    case -10: {
                        fw.write("CAPTURE ERROR: 7-day period has expired, transaction will be voided\n");
                        break;
                    }
                    case -11: {
                        fw.write("CAPTURE ERROR: Unknown error\n");
                        break;
                    }

                    //REFUND ERRORS
                    case -12: {
                        fw.write("REFUND ERROR: Transaction does not exist\n");
                        break;
                    }
                    case -13: {
                        fw.write("REFUND ERROR: Transaction has not been captured\n");
                        break;
                    }
                    case -14: {
                        fw.write("REFUND ERROR: Transaction already refunded\n");
                        break;
                    }
                    case -15: {
                        fw.write("REFUND ERROR: Refund amount is greater than the captured amount\n");
                        break;
                    }
                    case -16: {
                        fw.write("REFUND ERROR: Unknown error\n");
                        break;
                    }

                }

                fw.close();
            }else
                return false;

        }catch(Exception e)
        {
            System.out.println(e.getMessage());
            return false;
        }

        return true;
    }

    //capture funds
    public void updateTransactionState_AfterCapture(int returned_val_by_capture, long transactionId){
        if (returned_val_by_capture < 0 ){
            switch(returned_val_by_capture){
                case -1: //transaction does not exist
                    try {
                        transactionDatabase.deleteTransaction(transactionId);
                    }catch(Exception e){
                        log(2);
                    }
                    log(-8);
                    break;
                case -2: //transaction already captured
                    try {
                        transactionDatabase.updateTransactionStatus(transactionId, "Captured");
                    }catch(Exception e){
                        log(3);
                    }
                    log(-9);
                    break;
                case -3: //exists, but 7 day period has expired
                    try {
                        transactionDatabase.updateTransactionStatus(transactionId, "Void");
                    }catch(Exception e){
                        log(3);
                    }
                    log(-10);
                    break;
                case -4: //unknown error
                    log(-11);
            }
        }else{
            try {
                transactionDatabase.updateTransactionStatus(transactionId, "Captured");
            }catch(Exception e){
                log(3);
            }
        }
    }

    //get refund
    public void updateTransactionState_AfterRefund(int returned_val_by_refund, long transactionId){
        if (returned_val_by_refund < 0){
            switch(returned_val_by_refund){
                case -1: //does not exist
                    try {
                        transactionDatabase.deleteTransaction(transactionId);
                    }catch(Exception e){
                            log(2);
                    }
                    log(-12);
                    break;
                case -2: //exists but has not been captured
                    try {
                        transactionDatabase.updateTransactionStatus(transactionId, "Authorised");
                    }catch(Exception e){
                        log(3);
                    }
                    log(-13);
                    break;
                case -3: //already refunded
                    try {
                        transactionDatabase.updateTransactionStatus(transactionId, "Refunded");
                    }catch(Exception e){
                        log(3);
                    }
                    log(-14);
                    break;
                case -4: //refund amount greater than captured amount
                    try {
                        transactionDatabase.updateTransactionStatus(transactionId, "Captured");
                    }catch(Exception e){
                        log(3);
                    }
                    log(-15);
                    break;
                case -5: //unknown error
                    log(-16);
                    break;
            }
        }else{
            try {
                transactionDatabase.updateTransactionStatus(transactionId, "Refunded");
            }catch(Exception e){
                log(3);
            }
        }
    }

    public boolean isInUnverified(){
        return Unverified;
    }

    public boolean isInVerifiedOffline(){
        return Verified_offline;
    }

    public boolean isInAuthorised(){
        //if transactionId has been initialized (at least the auth() has been called on the transaction)
        if(transactionId!=null)
        {
            String state = transactionDatabase.getTransactionState(transactionId);

            return(state.equalsIgnoreCase("Authorised"));
        }else //if the transaction has not been assigned yet, so the auth() call was not made, then it is surely not in the Authorised state
            return false;
    }

    public boolean isInCaptured() {
        //if transactionId has been initialized (at least the auth() has been called on the transaction)
        if(transactionId!=null)
        {
            String state = transactionDatabase.getTransactionState(transactionId);

            return(state.equalsIgnoreCase("Captured"));
        }else
            return false;
    }

    public boolean isInVoid() {
        //if transactionId has been initialized (at least the auth() has been called on the transaction)
        if(transactionId!=null)
        {
            String state = transactionDatabase.getTransactionState(transactionId);

            return(state.equalsIgnoreCase("Void"));
        }else
            return false;
    }

    public boolean isInRefunded() {
        //if transactionId has been initialized (at least the auth() has been called on the transaction)
        if(transactionId!=null)
        {
            String state = transactionDatabase.getTransactionState(transactionId);

            return(state.equalsIgnoreCase("Refunded"));
        }else
            return false;
    }
}
