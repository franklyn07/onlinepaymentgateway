package cps3230.payment_processing_system.back_end;

import cps3230.payment_processing_system.back_end.Transaction;

import java.util.HashMap;
import java.util.Map;

public class TransactionDatabase {

    Map<Long, Transaction> database;

    public TransactionDatabase() {
        database = new HashMap<Long, Transaction>();
    }

    //constructor overriding to let you pass an already existing database for example
    public TransactionDatabase(Map<Long, Transaction> database){
        this.database = database;
    }

    public void saveTransaction(Transaction transaction) throws Exception {

        if(transaction == null) {
            throw new Exception("Transaction you are trying to save is null");
        }else {

            // try to get a transaction with the same id
            Transaction existingTransaction = database.get(transaction.get_id());

            if(existingTransaction != null)
            {
                throw new Exception();
            }else
            {
                /* Two scenarios are possible:
                    - the key already exists, but the value is null, so we set the new value.
                    - the key does not exist, so we put the new pair.
                 */
                database.put(transaction.get_id(), transaction);
            }
        }
    }

    public int getNumberOfTransactions(){
        return database.size();
    }

    public void deleteTransaction(long transactionId) throws Exception{

        Transaction return_val = database.remove(transactionId);

        if (return_val == null){
            throw new Exception("Transaction not found when trying to delete");
        }
    }

    public void updateTransactionStatus(long transactionId, String newStatus) throws Exception{
        Transaction returnedTransaction = database.get(transactionId);

        if(returnedTransaction != null){
            returnedTransaction.set_state(newStatus);
        }else{
            throw new Exception("Transaction not found when updating status");
        }
    }

    public String getTransactionState(long transactionId) {
        Transaction transaction = database.get(transactionId);

        return transaction.get_state();
    }
}
