package cps3230.payment_processing_system.back_end;

import cps3230.payment_processing_system.back_end.CCInfo;

public class Transaction {
    private long id;
    private CCInfo info;
    private long amount;
    private String state;

    //constructor - no need for setters since Transaction variables will not be altered
    public Transaction(long id, CCInfo info, long amount, String state){
        this.id = id;
        this.info = info;
        this.amount = amount;
        this.state = state;
    }

    //getters for variables
    public long get_id(){
        return id;
    }

    public CCInfo get_info(){
        return info;
    }

    public long get_amount(){
        return amount;
    }

    public String get_state(){
        return state;
    }

    //setter for status
    public void set_state(String newState) {state = newState;}
}
