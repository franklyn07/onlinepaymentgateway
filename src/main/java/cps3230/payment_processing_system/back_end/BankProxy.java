package cps3230.payment_processing_system.back_end;

import cps3230.payment_processing_system.back_end.CCInfo;

public interface BankProxy {
    public long auth(CCInfo info, long amount);

    public int capture(long transaction_id);

    public int refund(long transaction_id, long amount);
}
